# Kali i3-gaps | Custom ISO

[![CI/CD][CI/CD]][CI/CD URL]
[![LICENSE][LICENSE]][LICENSE URL]
[![Twitter][Twitter]][Twitter URL]
[![Github Sponsors][Github Sponsors]][Github Sponsors URL]
[![Ko-Fi][Ko-Fi]][Ko-Fi URL]

## Table of Contents
- [Introduction](#introduction)
- [Pictures](#pictures)
- [Installation](#installation)
  - [Downloading](#downloading)
  - [Building](#building)
    - [Kali Debian Based Systems](#kali-debian-based-systems)
    - [Non-Kali Debian Based Systems](#non-kali-debian-based-systems)
- [Known Issues](#known-issues)
- [Bug Reports](#bug-reports)
- [Keybinds](#keybinds)
- [Customization](#customization)
- [FAQ](#faq)
- [Donations](#donations)
- [Acknowledgements](#acknowledgements)

## Introduction
This is my take on Kali, using i3-gaps as a WM. I've done this because I loved
how i3-gaps worked on my new (well, it was new back in May 2019) laptop. It's
really lightweight (compared to other WM's and DE's) while being really
customizable and easy to use.

## Pictures

![1][1]

![2][2]

![3][3]

## Installation
### Downloading
As of 2022-08, Kali i3-gaps has a Gitlab CI/CD pipeline, allowing me to share 
ready-for-use images of Kali i3-gaps to everyone. You can download Kali i3-gaps 
and its build-log from the links below:
- [Kali i3-gaps ISO][Kali i3-gaps ISO]
- [Kali i3-gaps Build Log][Kali i3-gaps Build Log]

However, do constrains, the only version of the available image is `amd64`. Thus 
if you require `i386`/`arm64`/`armel`/`armhf` versions of Kali i3-gaps, please 
refer to [building](#building).

### Building
#### Kali Debian Based Systems
To build the ISO:

```
$ sudo apt install -y curl git live-build cdebootstrap
$ git clone https://gitlab.com/Arszilla/kali_i3-gaps.git
$ cd kali-i3_gaps
$ ./build.sh --variant i3_gaps --verbose
```

By default, the command above will build a Kali i3-gaps ISO based on your host's 
architecture. If you want another architecture (`i386`/`arm64`/`armel`/`armhf`), 
use the `--arch` flag:

```
$ ./build.sh --arch <i386/amd64/arm64/armel/armhf> --variant i3_gaps --verbose
```

#### Non-Kali Debian Based Systems
If you are on a non-Kali Debian based system, check out Kali's
[guide][Kali Docs] on building the ISO.

---

**Once the script is finished, your image should be in `~/kali-i3_gaps/images`.**

## Known Issues
- In VMs, (tested on `VMware`) `backend = "glx";` (Line 119 in `picom.conf`) causes 
the VMs to act erratically (lag, keyboard to not function properly, etc.). To fix 
this, enable 3D acceleration for the VM in your virtualization software.
- In Live System instances there is no wallpaper present.
- Once the ISO is built and ran/installed, there won't be user directories (in 
`$HOME` or in `root`). This is because `xdg-user-dirs-update` doesn't run (during 
build, live-system or install phase). A manual fix for this is to run 
`xdg-user-dirs-update` in a terminal once the you log into the system.

## Bug Reports
I've been working on this since late 2019-11. There might be some errors here
and there; bugs and/or features from official Kali I've missed. If that's the
case and you find a bug and/or a missing feature:

- Fork the repository
- Make changes
- Submit a PR

I'll evaluate the changes ASAP and get back to you.

## Keybinds
Keybinds are the most important part about this ISO and i3-gaps in general. Unless
you are familiar with i3-gaps, you might be confused on how to operate with it.
For the sake of newcomers, most vital keybinds are as follows:

- Mod key:                                      `Left Windows` key (Between `Left Ctrl` & `Left Alt`, usually)
- Launch a `urxvt` terminal:                    `Mod + Return`
- Launch `rofi`:                                `Mod + d`
- Launch Firefox:                               `Mod + F1`
- Launch `pcmanfm` file manager:                `Mod + F2`
- Launch `ranger` file manager:                 `Mod + F3`
- Switch workspaces:                            `Mod + 1-9`
- Move the current window to another workspace: `Mod + Shift + 1-9`
- Launch the system menu:                       `Mod + 0`
- Switch between tiling and floating windows:   `Mod + Shift + Space`
- Move the window:                              `Mod + Shift` and arrow keys(or `jkl;`)
- Resize the window:                            `Mod + R` then arrow keys (or `jkl;`)
- Take a fullscreen screenshot:                 `Mod + Print`
- Take a screenshot of a window:                `Mod + Shift + Print`
- Take a screenshot of an area:                 `Mod + Ctrl + Print`
- Restart `i3-gaps` in place:                   `Mod + Shift + r`

Rest of the keybinds could be found in `~/.config/i3/config`

## Customization
`i3-gaps` and other packages used in this tiling WM are *really* customizable, which
is one of the perfect aspects of it IMO. A summary on customization is as follows:

- `~/.config` holds the majority of the dotfiles/Customization
- `~/.config/i3/config` is i3-gaps' dotfile. You can change keybinds, execs,
look and feel as well as many more aspects of your i3-gaps here. You can make
your containers open in certain workspaces. For more information, check out
[i3wm docs][i3wm Docs].
- `~/.config/polybar/config.ini` is the `polybar` config. You can customize your 
bar to your liking. For more information, check out [polybar docs][polybar Docs].
- `~/.Xresources` is your `Xresources` file, where you can customize your 
`X client` applications. The changes here affect `urxvt`.
- `~/.config/picom/picom.conf` is the `picom` config. You can edit your
compositor's settings here. Fading, window transparency and blur.
- `~/.config/dunst/dunstrc` is the `dunst` dotfile. You can customize your
notification delivery system here.
- `~/.config/mimeapps.list` can be used to set the system-wide default
programs. By default, the system uses `vim` as a text editor, `sxiv` as an
image viewer, and Firefox as a default browser. See
`Default for foreign programs (user-speific)` in [this][Debian Docs] page for
more information.
- `lxappearance` can be used to customize your system-wide theme, font, look,
and more.

## FAQ
- To connect to a WiFi, you run `nmtui` in the terminal.
- To edit your power options, run `xfce4-power-manager` in `rofi`.
- To manage your current display or multiple monitors, run `arandr` in `rofi`. 
Alternatively, you can use `xrandr` the terminal.

## Donations
If you like Kali i3-gaps and would like to support me, please consider donating 
using the links below. Any amount is more than welcome as I will be using the 
funds for further projects, such as building a homelab to run `gitlab-runner` 
in my own environment, instead of on DigitalOcean.
- [Github Sponsors][Github Sponsors URL]
- [Ko-Fi][Ko-Fi URL]
- BTC:  `bc1qfp2a7pncxvq3s9qgtj0fp7k6v5rzy8g763u7uk`
- BCH:  `qz3s06xm9j6cj26qavstykwysf3xs92l3ymjpvut88`
- ETH:  `0x3FB9505DA434Ce308880261acbe56A4e321DdEFC`
- XLM:  `GD2ARSM32ZLYXWTL6WMRVCDNAVRRCDBG7GRHNEVGYF6UVOWSUL4GJKWO`
- DOGE: `DNPBgj2JVgYm17h8ybxkpYmC2LZmL91pUs`

## Acknowledgements
- [TJNull][TJNull] for encouraging me to do this, as well
as helping me out and being an amazing friend.
- The [Kali Team][Kali Team] for their work on 
[live-build-config][live-build-config], making this ISO and project possible.

[CI/CD]:                    https://img.shields.io/gitlab/pipeline-status/arszilla/kali_i3-gaps?branch=main&style=flat-square
[CI/CD URL]:                https://gitlab.com/arszilla/kali_i3-gaps/-/commits/main
[LICENSE]:                  https://img.shields.io/gitlab/license/arszilla/kali_i3-gaps?style=flat-square
[LICENSE URL]:              ./LICENSE
[Twitter]:                  https://img.shields.io/badge/Twitter-%231DA1F2.svg?style=flat-square&logo=Twitter&logoColor=white
[Twitter URL]:              https://twitter.com/arszilla
[Github Sponsors]:          https://img.shields.io/badge/sponsor-30363D?style=flat-square&logo=GitHub-Sponsors&logoColor=#EA4AAA
[Github Sponsors URL]:      https://github.com/sponsors/arszilla
[Ko-Fi]:                    https://img.shields.io/badge/Ko--fi-F16061?style=flat-square&logo=ko-fi&logoColor=white
[Ko-Fi URL]:                https://ko-fi.com/arszilla
[1]:                        /Pictures/1.png
[2]:                        /Pictures/2.png
[3]:                        /Pictures/3.png
[Kali i3-gaps ISO]:         https://kali.arszilla.com/download/kali_i3-gaps_amd64.iso
[Kali i3-gaps Build Log]:   https://kali.arszilla.com/download/kali_i3-gaps_amd64.log
[Kali Docs]:                https://www.kali.org/docs/development/live-build-a-custom-kali-iso/
[i3wm Docs]:                https://i3wm.org/docs/userguide.html
[Debian Docs]:              https://wiki.debian.org/DefaultWebBrowser
[TJNull]:                   https://twitter.com/TJ_Null
[Kali Team]:                https://www.kali.org/about-us/
[live-build-config]:        https://gitlab.com/kalilinux/build-scripts/live-build-config
